<?php

namespace Faker\Provider;

/**
 * Depends on image generation from http://placeimg.com/
 */
class Image extends Base
{
    protected static $categories = array(
        'any', 'animals', 'architecture', 'people', 'nature', 'tech'
    );

    protected static $filters = array('grayscale', 'sepia');


    /**
     * 生成将返回随机图像的URL
     *
     * @param integer $width    宽度
     * @param integer $height   高度
     * @param string $category  分类
     * @param string|null $filter
     * @param bool $https       是否使用https协议
     *
     * @return string
     * @example 'http://placeimg.com/640/480/any'
     *
     */
    public static function imageUrl($width = 640, $height = 480, $category = 'any', $filter = null, $https = false)
    {
        $url = ($https ? 'https' : 'http') . "://placeimg.com/{$width}/{$height}";
        if (!in_array($category, static::$categories)) {
            throw new \InvalidArgumentException(sprintf('Unknown image category "%s"', $category));
        }
        $url .= "/{$category}";
        if($filter){
            if (!in_array($filter, static::$filters)) {
                throw new \InvalidArgumentException(sprintf('Unknown image filter "%s"', $filter));
            }
            $url .= "/{$filter}";
        }
        return $url;
    }

    /**
     * Download a remote random image to disk and return its location
     *
     * Requires curl, or allow_url_fopen to be on in php.ini.
     *
     * @example '/path/to/dir/13b73edae8443990be1aa8f1a483bc27.jpg'
     */
    public static function image($dir = null, $width = 640, $height = 480, $category = null, $fullPath = true, $randomize = true, $word = null, $gray = false)
    {
        $dir = is_null($dir) ? sys_get_temp_dir() : $dir; // GNU/Linux / OS X / Windows compatible
        // Validate directory path
        if (!is_dir($dir) || !is_writable($dir)) {
            throw new \InvalidArgumentException(sprintf('Cannot write to directory "%s"', $dir));
        }

        // Generate a random filename. Use the server address so that a file
        // generated at the same time on a different server won't have a collision.
        $name = md5(uniqid(empty($_SERVER['SERVER_ADDR']) ? '' : $_SERVER['SERVER_ADDR'], true));
        $filename = $name . '.jpg';
        $filepath = $dir . DIRECTORY_SEPARATOR . $filename;

        $url = static::imageUrl($width, $height, $category, $randomize, $word, $gray);

        // save file
        if (function_exists('curl_exec')) {
            // use cURL
            $fp = fopen($filepath, 'w');
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_FILE, $fp);
            $success = curl_exec($ch) && curl_getinfo($ch, CURLINFO_HTTP_CODE) === 200;
            fclose($fp);
            curl_close($ch);

            if (!$success) {
                unlink($filepath);

                // could not contact the distant URL or HTTP error - fail silently.
                return false;
            }
        } elseif (ini_get('allow_url_fopen')) {
            // use remote fopen() via copy()
            $success = copy($url, $filepath);
        } else {
            return new \RuntimeException('The image formatter downloads an image from a remote HTTP server. Therefore, it requires that PHP can request remote hosts, either via cURL or fopen()');
        }

        return $fullPath ? $filepath : $filename;
    }
}
