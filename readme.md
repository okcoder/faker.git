# Faker

> 使用方法参考官方 https://github.com/fzaninotto/Faker
> 以下是修改过的内容

#### 生成图片
```
$faker = \Faker\Factory::create('zh_CN');
$faker->imageUrl(rand(100, 300), rand(100, 300));



/**
 * 生成将返回随机图像的URL
 *
 * @param integer $width    宽度
 * @param integer $height   高度
 * @param string $category  分类  'any', 'animals', 'architecture', 'people', 'nature', 'tech',
 * @param string|null $filter   'grayscale', 'sepia'
 * @param bool $https       是否使用https协议
 *
 * @return string
 * @example 'http://placeimg.com/640/480/any'
 *
 */
 public static function imageUrl($width = 640, $height = 480, $category = 'any', $filter = null, $https = false)
 {
     $url = ($https ? 'https' : 'http') . "://placeimg.com/{$width}/{$height}";
     if (!in_array($category, static::$categories)) {
         throw new \InvalidArgumentException(sprintf('Unknown image category "%s"', $category));
     }
     $url .= "/{$category}";
     if($filter){
         if (!in_array($filter, static::$filters)) {
             throw new \InvalidArgumentException(sprintf('Unknown image filter "%s"', $filter));
         }
         $url .= "/{$filter}";
     }
     return $url;
 }
```
